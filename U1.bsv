package U1;
    interface Blinky;
        method Bool blink();
        method Action start();
        method ActionValue#(int) stop();
    endinterface

    module mkBlinky(Blinky);
        Reg#(Bool) ctrl_on <- mkReg(False);
        Reg#(Bool) led_on <- mkReg(False);
        Reg#(int) blink_ctr <- mkReg(0);

        rule saus (led_on == False && ctrl_on == True);
            $display("AN!");
            led_on <= True;
            blink_ctr <= blink_ctr + 1;
        endrule
        rule san (led_on == True && ctrl_on == True);
            $display("AUS!");
            led_on <= False;
        endrule
        method Action start();
            blink_ctr <= 0; // der Counter muss auf 0 gesetz
            ctrl_on <= True;
        endmethod
        method ActionValue#(int) stop();
            ctrl_on <= False;
            led_on <= False;
            return blink_ctr;
        endmethod
        method Bool blink();
            return led_on;
        endmethod
    endmodule
endpackage