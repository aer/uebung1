package HelloAluTB;
    import HelloAlu ::*;
   // typedef enum { Mul,Div,Add,Sub,And,Or } AluOps deriving (Eq, Bits);
    module mkHelloAluTB();
        HelloAlu alu <- mkHelloAlu;
        Reg#(Bool) started <- mkReg(False);
        Reg#(UInt#(33)) cycles <- mkReg(0);
        Reg#(Bool)  called <- mkReg(False);

        rule start(!called);
            alu.setupCalculation(Mul, 12, 2);
            $display("Start");
            called <= True;
        endrule

        rule endmod(called);
            $display("RESULT: %d", alu.getResult);
            $finish();
        endrule
    endmodule
endpackage