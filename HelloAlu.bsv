package HelloAlu;
    typedef enum { Mul,Div,Add,Sub,And,Or} AluOps deriving (Eq, Bits);
    interface HelloAlu;
        method Action setupCalculation(AluOps op, Int#(32) a, Int#(32) b);
        method ActionValue#(Int#(32)) getResult();
    endinterface
    module mkHelloAlu(HelloAlu);
        Reg#(Int#(32)) _a <- mkReg(0);
        Reg#(Int#(32)) _b <- mkReg(0);
        Reg#(AluOps) _op <- mkReg(Mul);
        Reg#(Bool) isSet <- mkReg(False);
        // new Operand Register 
        Reg#(Bool) newOperand <- mkReg(False);

        method ActionValue#(Int#(32)) getResult(newOperand);
            let a = _a;
            let b = _b;
            newOperand <= False;
            let result = case(_op)
                            Mul: return a * b;
                            Div: return a / b;
                            Add: return a + b;
                            Sub: return a - b;
                            And: return a & b;
                            Or: return a | b;
                        endcase;
            return result;           
        endmethod
        method Action setupCalculation(AluOps op, Int#(32) a, Int#(32) b);
            newOperand <= True;
            _op <= op;
            _a <= a;
            _b <= b;
            isSet <= True; 
        endmethod  
    endmodule
endpackage